<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chess Board</title>
    <style>
        .main {
            width:98%;
            margin:10% auto;
            border: #7be9ff}
        .box{
            width:150px;
            height: 150px;
            background:green;
            text-align:center;
            display:inline-block;}
        .box span{display:block;
            padding-top:50%;
            margin-top:-9px;
            font-size:18px;}
        .box:(2n)th-child{background:#fff;}
    </style>
</head>
<body>
<h1 align="center">Chess Board</h1>
<div class="main" align="center">
    <?php
    for($i=1;$i<=8;$i++){
        for($j=1;$j<=8;$j++){
            if($i&1){
                if($j&1){
                    echo '<div style="background: #ffffff;" class="box"><span></span></div>';
                }
                else{
                    echo '<div style="background:navy;" class="box"><span></span></div>';
                }
            } else{
                if($j&1){
                    echo '<div style="background:navy;" class="box"><span></span></div>';
                }
                else{
                    echo '<div style="background:#ffffff;" class="box"><span></span></div>';
                }
            }
        }
    }
    ?>
</div>
</body>
</html>